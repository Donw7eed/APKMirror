package taco.apkmirror.activities

import android.content.Intent
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import taco.apkmirror.R
import taco.apkmirror.databinding.DialogEdittextBinding

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val dialogBinding = DialogEdittextBinding.inflate(layoutInflater)

        AlertDialog.Builder(this)
            .setTitle(R.string.search)
            .setView(dialogBinding.root)
            .setNegativeButton(android.R.string.cancel) { _, _ -> finish() }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val i = Intent(this@SearchActivity, MainActivity::class.java)
                i.putExtra(
                    "url",
                    "https://www.apkmirror.com/?s=" + dialogBinding.editText.text.toString()
                )
                startActivity(i)
                finish()
            }
            .show()
    }
}
